package com.gildedrose;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.IntStream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;


public class TextCharacterisationTest {
    
    private static final String LS = System.lineSeparator();
          
    @Test
    public void runCharacteristicTestFor100Days() {
        IntStream.rangeClosed(0, 100)
                .forEach(i -> runCharacterisationTest(i, "expected_text_" + i + "_days.txt"));
    }
    
    private void runCharacterisationTest(int day, String expectedResultFile) {
        String expected = generateTestTextData(day);
        
        Path expectedContentFilePath = Paths.get("src/test/resources/", expectedResultFile);
        try {
            assertEquals("Failed whilst testing day: " + day + " and file: " + expectedContentFilePath.toAbsolutePath(),
                    new String(Files.readAllBytes(expectedContentFilePath)), expected);
        } catch (IOException ex) {
            fail();
        }
    }   

    public static String generateTestTextData(int days) {
        StringBuilder str = new StringBuilder();
        
        str.append("OMGHAI!" + LS);
        
        Item[] items = new Item[] {
            new Item("+5 Dexterity Vest", 10, 20), //
            new Item("Aged Brie", 2, 0), //
            new Item("Elixir of the Mongoose", 5, 7), //
            new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
            new Item("Sulfuras, Hand of Ragnaros", -1, 80),
            new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
            new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
            new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
            // this conjured item does not work properly yet
            new Item("Conjured Mana Cake", 3, 6) };
        
        GildedRose app = new GildedRose(items);
        
        for (int i = 0; i < days; i++) {
            str.append("-------- day " + i + " --------" + LS);
            str.append("name, sellIn, quality" + LS);
            for (Item item : items) {
                str.append(item + LS);
            }
            str.append(LS);
            app.updateQuality();
        }
        return str.toString();
    }
    
}
