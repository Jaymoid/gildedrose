/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gildedrose;

import static com.gildedrose.TextCharacterisationTest.generateTestTextData;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.IntStream;
import static org.junit.Assert.fail;
import static org.junit.Assert.fail;
import static org.junit.Assert.fail;
import static org.junit.Assert.fail;

/**
 * Run this to generate some test data
 *
 * @author james
 */
public class DataFilesGeneratorForCharacterisationTests {

    public static void main(String[] args) {
        generate100DaysOfTestData();
    }

    private static void generate100DaysOfTestData() {
        IntStream.rangeClosed(0, 100)
                .forEach(i -> generateDaysFiles(i, "expected_text_" + i + "_days.txt"));
    }

    private static void generateDaysFiles(int day, String resultFileToGenerate) {
        String output = generateTestTextData(day);
        Path generatePath = Paths.get("src/test/resources/", resultFileToGenerate);
        try {
            Files.write(generatePath, output.getBytes());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
